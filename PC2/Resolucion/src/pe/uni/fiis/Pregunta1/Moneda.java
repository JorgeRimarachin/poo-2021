package pe.uni.fiis.Pregunta1;

public class Moneda {
    private String nombre; 
    private double valorDolar; // 1 (nombre) == (valorDolar) Dolares
    private double cantidad;

    public double convertirADolar( double valorDolar, double cantidad, String nombre){
        double dolares = valorDolar * cantidad;
        System.out.println("El valor en dolares que tiene es: "+dolares);
        return dolares;
    }

    public void convertirAMoneda(double dolares, double valorDolar, String nombre){
        System.out.println("Conviertiendo a "+nombre+ " resulta: "+ dolares/valorDolar);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getValorDolar() {
        return valorDolar;
    }

    public void setValorDolar(double valorDolar) {
        this.valorDolar = valorDolar;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    
    
}


