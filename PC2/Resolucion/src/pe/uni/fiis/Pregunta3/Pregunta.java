package pe.uni.fiis.Pregunta3;

import java.util.*;

public class Pregunta {
    private String interrogante;
    private String alt1;
    private String alt2;
    private String alt3;

    
    public Pregunta(String interrogante, String alt1, String alt2, String alt3) {
        this.interrogante = interrogante;
        this.alt1 = alt1;
        this.alt2 = alt2;
        this.alt3 = alt3;
    }
    public void Llenar (String alt1,String alt2, String alt3){
        private Set<String> respuestas = new HashSet();
        respuestas.add(alt1);
        respuestas.add(alt2);
        respuestas.add(alt2);
    }
    

    public String getInterrogante() {
        return interrogante;
    }

    public void setInterrogante(String interrogante) {
        this.interrogante = interrogante;
    }

    public String getAlt1() {
        return alt1;
    }

    public void setAlt1(String alt1) {
        this.alt1 = alt1;
    }

    public String getAlt2() {
        return alt2;
    }

    public void setAlt2(String alt2) {
        this.alt2 = alt2;
    }

    public String getAlt3() {
        return alt3;
    }

    public void setAlt3(String alt3) {
        this.alt3 = alt3;
    }

}

