package pe.uni.fiis.Pregunta3;

public class Aplicacion {
    public static void main(String[] args) {
        Pregunta p1 = new Pregunta("¿Qué establecimiento desea conocer en la UNI?","1.Museo","2.Biblioteca central","3.Coliseo");
        Pregunta p2 = new Pregunta("¿Qué facultad desea conocer?","1.FIIS","2.FIQM","3.FIA");
        Pregunta p3 = new Pregunta("¿Hace cuantos años visita la UNI?","1.Un año","2.Tres años","3.Más de tres años");
        
        p1.Llenar(p1.getAlt1(), p1.getAlt2(), p1.getAlt3());
        p2.Llenar(p2.getAlt1(), p2.getAlt2(), p2.getAlt3());
        p3.Llenar(p3.getAlt1(), p3.getAlt2(), p3.getAlt3());

        System.out.println();
    }
}
