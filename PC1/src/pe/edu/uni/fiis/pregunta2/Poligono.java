package pe.edu.uni.fiis.pregunta2;

public class Poligono {
    private float area;
    private float perimetro;
    private int cantidadDiagonales;
    public float getArea(){
        return area;
    }
    public void setArea(float area){
        this.area= area;
    }
    public float getPerimetro(){
        return perimetro;
    }
    public void setPerimetro(float perimetro){
        this.perimetro = perimetro;
    }
    public int getCantidadDiagonales(){
        return cantidadDiagonales;
    }
    public void setCantidadDiagonales(int cantidadDiagonales){
        this.cantidadDiagonales = cantidadDiagonales;
    }
}
