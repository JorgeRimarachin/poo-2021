package pe.edu.uni.fiis.pregunta1;

public class Cliente {
    private String genero;
    private String linea;
    public String getGenero(){
        return genero;
    }
    public void setGenero(String genero){
        this.genero = genero;
    }
    public String getLinea(){
        return linea;
    }
    public void setLinea(String linea){
        this.linea = linea;
    }
}
