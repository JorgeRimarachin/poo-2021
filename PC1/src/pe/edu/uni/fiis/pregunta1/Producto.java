package pe.edu.uni.fiis.pregunta1;

public class Producto {
    private String marca;
    private String modelo;
    private String tipo;
    private String genero;
    private String horma;
    private String material;
    private String temporada;
    private String paisOrigen;
    public String getMarca(){
        return marca;
    }
    public void setMarca(String marca){
        this.marca = marca;
    }
    public String getModelo(){
        return modelo;
    }
    public void setModelo(String modelo){
        this.modelo = modelo;
    }
    public String getTipo(){
        return tipo;
    }
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    public String getGenero(){
        return genero;
    }
    public void setGenero(String genero){
        this.genero = genero;
    }
    public String getHorma(){
        return horma;
    }
    public void setHorma(String horma){
        this.horma = horma;
    }
    public String getMaterial(){
        return material;
    }
    public void setMaterial(String material){
        this.material = material;
    }
    public String getTemporara(){
        return temporada;
    }
    public void setTemporada(String temporada){
        this.temporada = temporada;
    }
    public String getPaisOrigen(){
        return paisOrigen;
    }
    public void setPaisOrigen(String paisOrigen){
        this.paisOrigen = paisOrigen;
    }
}
