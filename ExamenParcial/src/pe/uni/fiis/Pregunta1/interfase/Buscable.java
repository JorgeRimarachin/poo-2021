package pe.uni.fiis.Pregunta1.interfase;

public interface Buscable {
    public void busquedaPorTitulo();
    public void busquedaPorAutor();
    public void busquedaPorcodigo();
}
