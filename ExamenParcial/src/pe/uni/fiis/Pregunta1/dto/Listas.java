package pe.uni.fiis.Pregunta1.dto;
import java.util.*;
public class Listas {
    List<String> lista = new ArrayList<>();

    public Listas(List<String> lista) {
        this.lista = lista;
    }

    public void implementar(List<String> array, String nuevoElemento){
        array.add(nuevoElemento);
    }

    public void buscar(List<String> array, String elemento){
        for (String iterador: array) {
            if (iterador.compareTo(elemento)>0){
                System.out.println(elemento);
            }
        }
    }
    
}
