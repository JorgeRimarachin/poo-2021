package pe.uni.fiis.Pregunta1.dto;

public class Datos {
    private String descripcion;
    private String editorial;
    private String anioPublicacion;
    private String tituloCapitulo;
    private String resumen;
    public Datos(String descripcion, String editorial, String anioPublicacion, String tituloCapitulo, String resumen) {
        this.descripcion = descripcion;
        this.editorial = editorial;
        this.anioPublicacion = anioPublicacion;
        this.tituloCapitulo = tituloCapitulo;
        this.resumen = resumen;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getEditorial() {
        return editorial;
    }
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
    public String getAnioPublicacion() {
        return anioPublicacion;
    }
    public void setAnioPublicacion(String anioPublicacion) {
        this.anioPublicacion = anioPublicacion;
    }
    public String getTituloCapitulo() {
        return tituloCapitulo;
    }
    public void setTituloCapitulo(String tituloCapitulo) {
        this.tituloCapitulo = tituloCapitulo;
    }
    public String getResumen() {
        return resumen;
    }
    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    
}
