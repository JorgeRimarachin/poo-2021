package pe.uni.fiis.Pregunta2.enumerador;

public class NivelEducativo {
    enum Nivel{
        PRIMARIA("Primaria"),SECUNDARIA("Secundaria"),SUPERIOR("Superior");

        private String nivel;

        private Nivel(String nivel) {
            this.nivel = nivel;
        }

        public String getNivel() {
            return nivel;
        }      
    }
}
