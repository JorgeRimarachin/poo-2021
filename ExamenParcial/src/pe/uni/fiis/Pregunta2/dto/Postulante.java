
package pe.uni.fiis.Pregunta2.dto;

import pe.uni.fiis.Pregunta2.interfase.Actualizador;

public class Postulante implements Actualizador {
    private String numeroDNI;
    private String nombres;
    private String apellidos;
    private String direccion;
    private String numeroTelefono;
    private Integer edad;
    private Integer notaEvaluacion;
    public Postulante(String numeroDNI, String nombres, String apellidos, String direccion, String numeroTelefono,
            Integer edad) {
        this.numeroDNI = numeroDNI;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.numeroTelefono = numeroTelefono;
        this.edad = edad;
        this.notaEvaluacion = 0;
    }
    
    
    public Integer getNotaEvaluacion() {
        return notaEvaluacion;
    }


    public void setNotaEvaluacion(Integer notaEvaluacion) {
        this.notaEvaluacion = notaEvaluacion;
    }

    
    public void actualizar(int nuevaNota) {
        notaEvaluacion = nuevaNota;
        System.out.println("La nota ha sido actualizada y ahora es: "+notaEvaluacion)
    }
    
}
