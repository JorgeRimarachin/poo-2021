public class Ordenamiento {
	
	public void ordenar(String[] array){
		for (int i = 0; i < array.length; i++){
			for (int j = i+1; j < array.length; j++){
				if(array[i].compareTo(array[j]) > 0){
					String auxiliar = array[j];
					array[j] = array[i];
					array[i] = auxiliar;
				}						
			}
		}
	}
	
	public static void main (String args[]){		
		String[] nombres = {"Jorge","Julio","Javier","Pablo","Alicia"};	
		Ordenamiento objeto1 = new Ordenamiento();
		objeto1.ordenar(nombres);
		for (int i = 0; i < nombres.length; i++){
			System.out.println(nombres[i]);
		}
	}
	
} 	