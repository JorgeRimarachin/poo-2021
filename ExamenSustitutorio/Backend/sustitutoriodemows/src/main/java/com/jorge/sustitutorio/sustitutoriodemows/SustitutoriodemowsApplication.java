package com.jorge.sustitutorio.sustitutoriodemows;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SustitutoriodemowsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SustitutoriodemowsApplication.class, args);
	}

}
