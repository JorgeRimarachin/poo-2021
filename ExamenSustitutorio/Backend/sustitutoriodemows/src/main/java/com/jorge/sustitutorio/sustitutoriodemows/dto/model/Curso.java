package com.jorge.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class Curso {

//     CREATE TABLE curso (
// 	id_curso NUMERIC(9) PRIMARY KEY,
// 	nombre VARCHAR(200),
// 	precio NUMERIC(9,2),
// 	fecha_inicio VARCHAR(10),
// 	fecha_fin VARCHAR(10)
// );

    private Integer idCurso;
    private String nombre;
    private Integer precio;
    private String fechaInicio;
    private String fechaFin;
    
    public Curso(Integer idCurso, String nombre, Integer precio, String fechaInicio, String fechaFin) {
        this.idCurso = idCurso;
        this.nombre = nombre;
        this.precio = precio;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }
}
