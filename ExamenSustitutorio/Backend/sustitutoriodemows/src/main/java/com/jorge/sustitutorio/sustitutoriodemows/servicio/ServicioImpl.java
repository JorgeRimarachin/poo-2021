package com.jorge.sustitutorio.sustitutoriodemows.servicio;

import com.jorge.sustitutorio.sustitutoriodemows.dao.Dao;
import com.jorge.sustitutorio.sustitutoriodemows.dto.model.Estudiante;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public Estudiante autenticarEstudiante(Estudiante estudiante) {
        
        return dao.autenticarEstudiante(estudiante);
    }
   
    public Estudiante mostrarCursos(Estudiante estudiante) {
     
        return null;
    }
    
}
