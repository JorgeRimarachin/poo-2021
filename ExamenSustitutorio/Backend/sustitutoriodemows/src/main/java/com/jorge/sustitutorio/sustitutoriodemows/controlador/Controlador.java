package com.jorge.sustitutorio.sustitutoriodemows.controlador;

import com.jorge.sustitutorio.sustitutoriodemows.dto.model.Estudiante;
import com.jorge.sustitutorio.sustitutoriodemows.servicio.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    
    @Autowired
    private Servicio servicio;
    @RequestMapping(
            value = "autenticarEstudiante",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    
    public @ResponseBody Estudiante autenticarEstudiante(@RequestBody Estudiante estudiante){
        if(estudiante == null){
            return new Estudiante();
        }
        else{
            return servicio.autenticarEstudiante(estudiante);
        }
    }
}
