package com.jorge.sustitutorio.sustitutoriodemows.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.jorge.sustitutorio.sustitutoriodemows.dto.model.Estudiante;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DaoImpl implements Dao{
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Estudiante autenticarEstudiante(Estudiante estudiante) {
        String SQL = " SELECT C.nombre,C.precio,C.fecha_inicio,C.fecha_fin "
        + " FROM estudiante E "
        + " JOIN inscripcion_curso I ON (E.id_estudiante = I.id_estudiante) "
        + " JOIN curso C ON (I.id_curso = C.id_curso) "
        + " WHERE correo=? AND clave=? ";
        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1,estudiante.getCorreo());
            sentencia.setString(2,estudiante.getClave());
            ResultSet resultado = sentencia.executeQuery();
            estudiante = new Estudiante();
            while(resultado.next()){
                estudiante = extraerEstudiante(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return estudiante;
    }

    private Estudiante extraerEstudiante(ResultSet resultado) throws SQLException {
        Estudiante retorno = new Estudiante(
            resultado.getInt("C.precio"),
            resultado.getString("C.nombre"),
            resultado.getString("C.fecha_inicio"),
            resultado.getString("C.fecha_fin"),
                null
            );
        return retorno;
    }
    
    public Estudiante mostrarCursos(Estudiante estudiante) {
        String SQL = " SELECT nombre,precio,fecha_inicio,fecha_fin FROM curso ";
        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1,estudiante.getCorreo());
            sentencia.setString(2,estudiante.getClave());
            ResultSet resultado = sentencia.executeQuery();
            estudiante = new Estudiante();
            while(resultado.next()){
                estudiante = extraerEstudiante(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return estudiante;
    }

}
