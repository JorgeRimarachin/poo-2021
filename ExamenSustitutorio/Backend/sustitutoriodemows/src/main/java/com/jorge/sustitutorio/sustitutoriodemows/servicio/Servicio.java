package com.jorge.sustitutorio.sustitutoriodemows.servicio;

import com.jorge.sustitutorio.sustitutoriodemows.dto.model.Estudiante;

public interface Servicio {
    public Estudiante autenticarEstudiante(Estudiante usuario);
    public Estudiante mostrarCursos(Estudiante estudiante);
}
