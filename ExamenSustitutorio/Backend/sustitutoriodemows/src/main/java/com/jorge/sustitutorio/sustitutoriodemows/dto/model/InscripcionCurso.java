package com.jorge.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class InscripcionCurso {
    
//     CREATE TABLE inscripcion_curso(
// 	id_curso NUMERIC(9),
// 	id_estudiante NUMERIC(9),
// 	fecha_inscripcion VARCHAR(10)	
// );

    private Integer idCurso;
    private Integer idEstudiante;
    private String fechaInscripcion;
    
    public InscripcionCurso(Integer idCurso, Integer idEstudiante, String fechaInscripcion) {
        this.idCurso = idCurso;
        this.idEstudiante = idEstudiante;
        this.fechaInscripcion = fechaInscripcion;
    }    
}
