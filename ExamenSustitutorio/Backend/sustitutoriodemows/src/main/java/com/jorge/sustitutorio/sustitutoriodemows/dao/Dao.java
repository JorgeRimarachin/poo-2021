package com.jorge.sustitutorio.sustitutoriodemows.dao;

import com.jorge.sustitutorio.sustitutoriodemows.dto.model.Estudiante;

public interface Dao {
    public Estudiante autenticarEstudiante(Estudiante usuario);
    public Estudiante mostrarCursos(Estudiante estudiante);
}
