package com.jorge.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class Estudiante {
    
    // CREATE TABLE estudiante(
	// id_estudiante NUMERIC(9) PRIMARY KEY, 
	// nombres VARCHAR(200),
	// apellidos VARCHAR(200),
	// correo VARCHAR(200),
	// clave VARCHAR(200)
    // );  

	private Integer idEstudiante;
	private String nombres;
	private String apellidos;
	private String correo;
	private String clave;
	
	public Estudiante(Integer idEstudiante, String nombres, String apellidos, String correo, String clave) {
		this.idEstudiante = idEstudiante;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.correo = correo;
		this.clave = clave;
	}

	public Estudiante() {
    }
}
