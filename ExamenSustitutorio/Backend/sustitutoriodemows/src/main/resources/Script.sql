USE poo

CREATE TABLE estudiante(
	id_estudiante NUMERIC(9) PRIMARY KEY, 
	nombres VARCHAR(200),
	apellidos VARCHAR(200),
	correo VARCHAR(200),
	clave VARCHAR(200)
);

CREATE TABLE curso (
	id_curso NUMERIC(9) PRIMARY KEY,
	nombre VARCHAR(200),
	precio NUMERIC(9,2),
	fecha_inicio VARCHAR(10),
	fecha_fin VARCHAR(10)
);

CREATE TABLE inscripcion_curso(
	id_curso NUMERIC(9),
	id_estudiante NUMERIC(9),
	fecha_inscripcion VARCHAR(10)	
);

INSERT INTO estudiante(id_estudiante,nombres,apellidos,correo,clave) VALUES(1,'jorge','rimarachin','jorge@uni.pe','hola');
INSERT INTO estudiante(id_estudiante,nombres,apellidos,correo,clave) VALUES(2,'luis','chuquiyauri','luis@uni.pe','adios');

INSERT INTO curso(id_curso,nombre,precio,fecha_inicio,fecha_fin) VALUES(100,'POO',10,'15/05/21','15/06/21');
INSERT INTO curso(id_curso,nombre,precio,fecha_inicio,fecha_fin) VALUES(101,'TSCA',10,'16/05/21','16/06/21');

INSERT INTO inscripcion_curso(id_curso,id_estudiante,fecha_inscripcion) VALUES(100,1,'10/05/21');
INSERT INTO inscripcion_curso(id_curso,id_estudiante,fecha_inscripcion) VALUES(101,2,'11/05/21');
INSERT INTO inscripcion_curso(id_curso,id_estudiante,fecha_inscripcion) VALUES(101,1,'10/05/21');
INSERT INTO inscripcion_curso(id_curso,id_estudiante,fecha_inscripcion) VALUES(100,2,'11/05/21');

SELECT C.nombre,C.precio,C.fecha_inicio,C.fecha_fin
FROM estudiante E
JOIN inscripcion_curso I ON (E.id_estudiante = I.id_estudiante)
JOIN curso C ON (I.id_curso = C.id_curso)
WHERE correo = 'jorge@uni.pe' AND clave = 'hola';

SELECT nombre,precio,fecha_inicio,fecha_fin
FROM curso;