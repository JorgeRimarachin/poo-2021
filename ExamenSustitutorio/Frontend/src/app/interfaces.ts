export interface Autenticable{
	correo:string;
	clave:string;
}

export interface Curso{
	nombre:string;
	precio:number;
	fechaInicio:string;
	fechaFin:string;
}

export interface ListaCursos{
	cursos:Curso[];
}