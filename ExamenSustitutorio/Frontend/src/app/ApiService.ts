import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import {ListaCursos, Autenticable} from "./interfaces";
import { retry, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json;charset=utf-8'
        })
    };
    errorHandl(error: any) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } 
        else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }    
        console.log(errorMessage);
        return throwError(errorMessage);
    }
    constructor(private http: HttpClient) { }  
    selectCursos():Observable<ListaCursos>{
        return this.http.post<ListaCursos>("http://localhost:8080/selectCursos", null, this.httpOptions).pipe(
            retry(1),catchError(this.errorHandl)
        );
    }
    
    autenticar(autenticable:Autenticable):Observable<ListaCursos>{
        return this.http.post<ListaCursos>("http://localhost:8080/autenticar", autenticable, this.httpOptions).pipe(
            retry(1),catchError(this.errorHandl)
        );
    }
}