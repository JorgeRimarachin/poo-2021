import { Component, OnInit } from '@angular/core';
import {ApiService} from '../ApiService';
import {Curso, Autenticable} from '../interfaces';

@Component({
  selector: 'app-autenticacion',
  templateUrl: './autenticacion.component.html',
  styleUrls: ['./autenticacion.component.scss']
})
export class AutenticacionComponent implements OnInit {

  autenticado:boolean = false;
	autenticable:Autenticable = <Autenticable>{};
	cursos:Curso[] = [];
	
	constructor(private api:ApiService){}
	
	ngOnInit(): void {
	}
	
	autenticar():void{
		this.api.autenticar(this.autenticable).subscribe(data=>{
			if(data != null)
			{
				for(let i:number = 0; i<data.cursos.length; i++)
				{
					this.cursos.push(data.cursos[i]);
				}
				this.autenticado = true;
			}}
		
		)
	}

}
