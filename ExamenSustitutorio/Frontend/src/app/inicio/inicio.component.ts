import { Component, OnInit } from '@angular/core';
import {ApiService} from '../ApiService';
import {Curso} from '../interfaces';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  cursos:Curso[] = [];
	
	constructor(private api:ApiService){}
	
	ngOnInit(): void {
		this.api.selectCursos().subscribe(data => {
			for(let i:number = 0; i<data.cursos.length; i++)
			{
				this.cursos.push(data.cursos[i]);
			}
		})
	}

}
