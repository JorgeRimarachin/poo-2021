import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuscadorComponent } from './buscador/buscador.component';
// import { LoginComponent } from './login/login.component';
import {InicioComponent} from './inicio/inicio.component';
import {AutenticacionComponent} from './autenticacion/autenticacion.component';

const routes: Routes = [
  // {path: "", component: LoginComponent},
  {path: "buscador", component: BuscadorComponent},
  {path:"", component:InicioComponent},
	{path:"autenticacion", component:AutenticacionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
