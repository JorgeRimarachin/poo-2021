CREATE DATABASE poo;

USE poo;

-- creacion de tablas

CREATE TABLE asignacion (
	id_asignacion NUMERIC(5) PRIMARY KEY,
	codigo_empleado VARCHAR(20),
	id_actividad NUMERIC(4), 
	presupuesto NUMERIC(9,2)
);

CREATE TABLE actividad (
	id_actividad NUMERIC(4) PRIMARY KEY, 
	nombre VARCHAR(50),
	prioridad NUMERIC(1)
);

CREATE TABLE empleado (
	codigo_empleado VARCHAR(20) PRIMARY KEY,
	codigo_departamento VARCHAR(10),
	nombres VARCHAR(50), 
	apellidos VARCHAR(100)
);

CREATE TABLE departamento (
	codigo_departamento VARCHAR(10) PRIMARY KEY,
	nombre VARCHAR(100),
	ubicacion VARCHAR(50)
);

-- llaves foráneas

ALTER TABLE asignacion ADD FOREIGN KEY (id_actividad) REFERENCES actividad(id_actividad);

ALTER TABLE asignacion ADD FOREIGN KEY (codigo_empleado) REFERENCES empleado(codigo_empleado);

ALTER TABLE empleado ADD FOREIGN KEY (codigo_departamento) REFERENCES departamento(codigo_departamento);

-- asignacion de registros

INSERT INTO actividad(id_actividad,nombre,prioridad) VALUES (1,'reporte',1);
INSERT INTO actividad(id_actividad,nombre,prioridad) VALUES (2,'reunion',1);
INSERT INTO actividad(id_actividad,nombre,prioridad) VALUES (3,'simulacro',2);
INSERT INTO actividad(id_actividad,nombre,prioridad) VALUES (4,'descanso',2);

INSERT INTO departamento(codigo_departamento,nombre,ubicacion) VALUES ('1001','finanzas','piso1');
INSERT INTO departamento(codigo_departamento,nombre,ubicacion) VALUES ('1002','contabilidad','piso2');
INSERT INTO departamento(codigo_departamento,nombre,ubicacion) VALUES ('1003','RRHH','piso3');

INSERT INTO empleado(codigo_empleado,codigo_departamento,nombres,apellidos) VALUES ('100','1002','jorge','rimarachin');
INSERT INTO empleado(codigo_empleado,codigo_departamento,nombres,apellidos) VALUES ('101','1003','luis','chuquiyauri');

INSERT INTO asignacion(id_asignacion,codigo_empleado,id_actividad,presupuesto) VALUES (1,'100',4,50.00);
INSERT INTO asignacion(id_asignacion,codigo_empleado,id_actividad,presupuesto) VALUES (2,'101',1,600.00);
INSERT INTO asignacion(id_asignacion,codigo_empleado,id_actividad,presupuesto) VALUES (3,'100',2,700.00);

-- 1

SELECT E.nombres, E.apellidos, D.nombre AS nombre_departamento
FROM empleado E
JOIN departamento D ON (D.codigo_departamento = E.codigo_departamento);

-- 2

INSERT INTO actividad(id_actividad,nombre,prioridad) VALUES (5,'viaje',2);

-- 3

SELECT Asi.codigo_empleado , Ac.nombre AS Actividades
FROM asignacion Asi
JOIN actividad Ac ON (Asi.id_actividad = Ac.id_actividad)
WHERE codigo_empleado = '100';

-- 4

UPDATE departamento 
SET ubicacion = 'sotano'
WHERE codigo_departamento = 1002;
