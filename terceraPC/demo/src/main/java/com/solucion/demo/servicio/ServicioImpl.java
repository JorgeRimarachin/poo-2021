package com.solucion.demo.servicio;

import java.util.List;

import com.solucion.demo.dto.Actividad;
import com.solucion.demo.dto.Asignacion;
import com.solucion.demo.dto.Departamento;
import com.solucion.demo.dto.Empleado;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional

public class ServicioImpl implements Servicio{

   
    public List<Empleado> obtenerEmpleados() {
        
        return null;
    }


    public List<Asignacion> obtenerAsignaciones(Empleado empleado) {
        
        return null;
    }


    public Actividad agregarActividad(Actividad actividad) {
        
        return null;
    }


    public Departamento actualizarDepartamento(Departamento departamento) {
        
        return null;
    }
    
}
