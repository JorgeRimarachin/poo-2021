package com.solucion.demo.dto;

import java.util.List;

import lombok.Data;

@Data
public class RespuestaEmpleados {
    private List<Empleado> lista;
}
