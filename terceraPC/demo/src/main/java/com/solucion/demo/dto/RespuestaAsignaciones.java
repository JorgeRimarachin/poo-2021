package com.solucion.demo.dto;

import java.util.List;

import lombok.Data;

@Data
public class RespuestaAsignaciones {
    private List<Asignacion> lista;
}
