package com.solucion.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.solucion.demo.dto.Actividad;
import com.solucion.demo.dto.Asignacion;
import com.solucion.demo.dto.Departamento;
import com.solucion.demo.dto.Empleado;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void crearConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void cerrarConexion(){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Departamento actualizarDepartamento(Departamento departamento) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public Actividad agregarActividad(Actividad actividad) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public List<Asignacion> obtenerAsignaciones(Empleado empleado) {
        
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT Asi.codigo_empleado, Ac.nombre AS Actividades ").
                    append(" FROM asignacion Asi ").
                    append(" JOIN actividad Ac ON (Asi.id_actividad = Ac.id_actividad) ").
                    append(" WHERE codigo_empleado = '?' ");
        
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sb.toString());
            sentencia.setString(1, );;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    
    public List<Empleado> obtenerEmpleados() {
        
        return null;
    }
    
}
