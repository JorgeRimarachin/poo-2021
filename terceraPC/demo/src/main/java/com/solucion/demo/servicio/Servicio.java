package com.solucion.demo.servicio;

import java.util.List;

import com.solucion.demo.dto.Actividad;
import com.solucion.demo.dto.Asignacion;
import com.solucion.demo.dto.Departamento;
import com.solucion.demo.dto.Empleado;

public interface Servicio {
    public List<Empleado> obtenerEmpleados();
    public List<Asignacion> obtenerAsignaciones(Empleado empleado);
    public Actividad agregarActividad(Actividad actividad);
    public Departamento actualizarDepartamento(Departamento departamento);
}
