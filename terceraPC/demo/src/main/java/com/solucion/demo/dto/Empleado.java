package com.solucion.demo.dto;

import lombok.Data;

@Data
public class Empleado {
    private String codigo_empleado;
	private String codigo_departamento;
	private String nombres;
	private String apellidos;
}
