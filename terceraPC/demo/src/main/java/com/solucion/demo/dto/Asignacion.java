package com.solucion.demo.dto;

import lombok.Data;

@Data
public class Asignacion {
    private Integer id_asignacion;
	private String codigo_empleado;
	private Integer id_actividad;
	private Integer presupuesto;
}
