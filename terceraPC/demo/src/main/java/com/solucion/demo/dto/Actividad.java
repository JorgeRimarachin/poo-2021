package com.solucion.demo.dto;

import lombok.Data;

@Data
public class Actividad {
    private Integer id_actividad;
	private String nombre;
	private Integer prioridad;
}
