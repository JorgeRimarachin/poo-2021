import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../ApiService';
import { Router } from '@angular/router';
import {CargaScriptsService} from '../cargaScrips.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  email: string = '';
  password: string = '';
  mensaje: string = '';
  error: string = "";
  form?: FormGroup;
  usuarioLogin?: SesionUsuario;
  usuario?: Usuario;

  ngOnInit(): void {
  }

  ingresar(): void{
    let usuarioLogin: SesionUsuario;

    usuarioLogin = {
      email: this.email,
      contraseña: this.password
    }

    this.api.verificarUsuario(usuarioLogin).subscribe( data => {
      if(data){
        this.usuario = data;
        this.router.navigate(['home']);
      }
    },err => {this.mensaje = err;});
    console.log('Usuario: '+ this.email);
    console.log('Contraseña: '+ this.password);
  }

}
