package com.electronica.backend.electronicaws.controlador;

import com.electronica.backend.electronicaws.dto.RespuestaAdministrativos;
import com.electronica.backend.electronicaws.dto.Usuario;
import com.electronica.backend.electronicaws.servicio.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {

    @Autowired
    private Servicio servicio;

    @RequestMapping(
        value =  "/verificar-usuario",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )

    public @ResponseBody Usuario autenticarUsuario(@RequestBody Usuario usuario){
        
        RespuestaAdministrativos respuestaAdministrativos = new RespuestaAdministrativos();
        respuestaAdministrativos.setLista(servicio.autenticarAdministrativo(usuario));
        return respuestaAdministrativos;
    }
}
