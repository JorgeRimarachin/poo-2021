package com.electronica.backend.electronicaws.dao;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.electronica.backend.electronicaws.dto.Usuario;



public interface Dao {
    public Usuario autenticarAdministrativo(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
    public Usuario extraerUsuario(ResultSet resultSet) throws SQLException;
}
