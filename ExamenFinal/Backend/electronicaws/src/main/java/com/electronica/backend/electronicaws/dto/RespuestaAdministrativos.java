package com.electronica.backend.electronicaws.dto;

import java.util.List;

import lombok.Data;

@Data
public class RespuestaAdministrativos {
    List<Usuario> lista; 
}
