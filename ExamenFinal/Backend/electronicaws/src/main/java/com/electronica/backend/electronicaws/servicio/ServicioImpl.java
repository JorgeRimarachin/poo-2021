package com.electronica.backend.electronicaws.servicio;

import java.sql.ResultSet;
import java.sql.SQLException;


import com.electronica.backend.electronicaws.dao.Dao;
import com.electronica.backend.electronicaws.dto.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    
    public Usuario autenticarAdministrativo(Usuario usuario) {
        return dao.autenticarAdministrativo(usuario);
    }

    public Usuario extraerUsuario(ResultSet resultSet) throws SQLException {

        return null;
    }

    public Usuario actualizarUsuario(Usuario usuario) {
        return null;
    }
    
}
