package com.electronica.backend.electronicaws.dto;

import java.sql.ResultSet;

import lombok.Data;

@Data
public class Usuario {
    private Integer id;
	private String nombres;
	private String apellidos;
	private String correo;
	private String administrador;
	private String clave;
    
    public Usuario(Integer id, String nombres, String apellidos, String correo, String administrador, String clave) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.administrador = administrador;
        this.clave = clave;
    }

    public void extraerUsuario(ResultSet resultSet) {
    }
    
    
}
