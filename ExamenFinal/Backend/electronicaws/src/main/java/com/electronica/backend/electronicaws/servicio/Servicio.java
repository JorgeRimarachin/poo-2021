package com.electronica.backend.electronicaws.servicio;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.electronica.backend.electronicaws.dto.Usuario;


public interface Servicio {
    public Usuario autenticarAdministrativo(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
    public Usuario extraerUsuario(ResultSet resultSet) throws SQLException;
}
