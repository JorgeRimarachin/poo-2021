package com.electronica.backend.electronicaws.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import com.electronica.backend.electronicaws.dto.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DaoImpl implements Dao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Usuario autenticarAdministrativo(Usuario usuario) {
        
        String SQL = "SELECT id,nombres,apellidos,correo,administrador,clave FROM usuario WHERE administrador = ?";
        try {
            obtenerConexion();
            PreparedStatement preparedStatement = conexion.prepareStatement(SQL);
            preparedStatement.setString(1, usuario.getAdministrador());

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                usuario.extraerUsuario(resultSet);
            }
            resultSet.close();
            preparedStatement.close();
            cerrarConexion(resultSet,preparedStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;
    }

    public Usuario extraerUsuario(ResultSet resultSet) throws SQLException{
        Usuario usuario = new Usuario(
            resultSet.getInt("id"),
            resultSet.getString("nombres"), 
            resultSet.getString("apellidos"),
            resultSet.getString("correo"),
            resultSet.getString("administrador"),
            resultSet.getString("clave"));
        return usuario;
    }

    public Usuario actualizarUsuario(Usuario usuario){
        String SQL = "UPDATE usuario SET id_usuario = ?, nombres=?, apellidos=?, correo=?, administrador=?, clave=? WHERE administrador = 1";

        try {
            obtenerConexion();
            PreparedStatement preparedStatement = conexion.prepareStatement(SQL);
            preparedStatement.setString(1, usuario.getAdministrador());

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                usuario.extraerUsuario(resultSet);
            }
            resultSet.close();
            preparedStatement.close();
            cerrarConexion(resultSet,preparedStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usuario;
    }
}

    
