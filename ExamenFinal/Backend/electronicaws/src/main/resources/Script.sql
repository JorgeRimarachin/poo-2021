CREATE DATABASE poo;

USE poo;

CREATE TABLE usuario(
	id_usuario NUMERIC(9),
	nombres VARCHAR(100),
	apellidos VARCHAR(100),
	correo VARCHAR(100),
	administrador CHAR(1),
	clave VARCHAR (200)
);

INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave) 
VALUES (1,'Jorge','Rimarachin','alguien@gmail.com',1,'abcd');
INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave) 
VALUES (2,'Luis','Chuquiyauri','alguien@gmail.com',0,'');
INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave) 
VALUES (3,'Abel','Garcia','alguien@gmail.com',1,'abcdef');



SELECT id_usuario,nombres,apellidos,correo,administrador,clave
FROM usuario
WHERE administrador = 1;

UPDATE usuario 
SET id_usuario = ?, nombres=?, apellidos=?, correo=?, administrador=?, clave=?
WHERE administrador = 1;